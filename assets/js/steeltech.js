"use strict";
var steeltech = {};

/**
 * Constants to use in this project, e.g. id´s
 * @type {{ID_HOME, ID_NEWS, ID_REQUIREMENTS, ID_APPLICATIONS, ID_FLY_SAFE, ID_HEADER_NAV, DEBUG}}
 */
steeltech.CONSTANTS = (function() {
    /**
     * Constants, please do NOT change values of them
     */
    return {
        ID_HOME: "home",
        ID_NEWS: "news",
        ID_REQUIREMENTS : "requirements",
        ID_APPLICATIONS : "applications",
        ID_FLY_SAFE : "flySafe",
        ID_HEADER_NAV : "headerNavbar",
        DEBUG : true
    }
})();

/**
 * objects for the ScrollMagic of this page!
 * @type {{ScrollEffect}}
 */
steeltech.ScrollMagic = (function() {
    /**
     *
     * @param id
     * @constructor
     */
    function ScrollEffect(id) {
        this.id = id;
        this.elem = document.getElementById(id);
    }
    
    /**
     * set Controller of this scene
     * @param controller
     */
    ScrollEffect.prototype.setController = function(controller) {
        this.controller = controller;
    };
    
    /**
     * create scene and add it to controller
     * http://scrollmagic.io/docs/ScrollMagic.Scene.html#constructor
     */
    ScrollEffect.prototype.createScene = function() {
        this.scene = new ScrollMagic.Scene({
            triggerElement: this.elem
        });
        
        this.scene.addTo(this.controller);
    };
    
    /**
     * set pin
     * http://scrollmagic.io/docs/ScrollMagic.Scene.html#setPin
     */
    ScrollEffect.prototype.setPin = function() {
        this.scene.setPin(this.elem, {
            pushFollowers: false
        });
    };
    
    /**
     * for debugging purpose only: add indicators to show start/end of triggers
     */
    ScrollEffect.prototype.addIndicators = function() {
        this.scene.addIndicators();
    };
    
    /**
     * set active class for menu bar
     * http://scrollmagic.io/docs/ScrollMagic.Scene.html#setClassToggle
     */
    ScrollEffect.prototype.markActive = function() {
        this.scene.setClassToggle("a[href^='#" + this.id + "']", "active");
    };
    
    return {
        ScrollEffect: ScrollEffect
    }
})();

/**
 * let start the magic!
 * @type {{start}}
 */
steeltech.init = (function() {
    var CONSTANT = steeltech.CONSTANTS;
    var magic = steeltech.ScrollMagic;
    
    var landingController;
    
    /**
     * place for all functions to do after page start
     */
    function start() {
        createController();
        
        createScrollEffect(CONSTANT.ID_HOME, true);
        createScrollEffect(CONSTANT.ID_NEWS, true);
        createScrollEffect(CONSTANT.ID_REQUIREMENTS, true);
        createScrollEffect(CONSTANT.ID_APPLICATIONS, true);
        createScrollEffect(CONSTANT.ID_FLY_SAFE);
        
        scrollToSectionAfterLoad();
        
        document.getElementById(CONSTANT.ID_HEADER_NAV).addEventListener("click", scrollingAnchors);
        
    }
    
    /**
     * create the ScrollMagic Controller
     */
    function createController() {
        /**
         * init the ScrollMagic Controller
         * @type {ScrollMagic.Controller}
         */
        landingController = new ScrollMagic.Controller(
            {
                globalSceneOptions: {
                    triggerHook: "onLeave",
                    duration: "100%"
                }
            }
        );
    
        /**
         * change the scroll behavior after clicking on an anchor, so it´s the same as normal scrolling
         */
        landingController.scrollTo(function (newPos) {
           TweenMax.to(window, 0.5, {
               scrollTo: {
                   y: newPos
               }
           });
        });
    }
    
    /**
     * create the scenes for each section
     * @param id
     * @param active
     */
    function createScrollEffect(id, active) {
        var scrollEffect = new magic.ScrollEffect(id);
        scrollEffect.setController(landingController);
        scrollEffect.createScene();
        scrollEffect.setPin();
    
        /**
         * if true, it means there is a menu link for this section
         */
        if(typeof active !== "undefined" && active) {
            scrollEffect.markActive();
        }
    
        /**
         * add indicator´s if we are still debugging
         */
        if(CONSTANT.DEBUG) {
            scrollEffect.addIndicators();
        }
    }
    
    /**
     * listen for clicks in nav, and if an anchor was selected make it scroll
     * @param event
     */
    function scrollingAnchors(event) {
        var elem = event.target;
    
        /**
         * do nothing if clicked element isn't a link
         * e.g. steeltech logo
         */
        if(elem.nodeName.toLowerCase() !== "a") {
            return;
        }
    
        var id = elem.getAttribute("href");
    
        /**
         * do nothing if link hasn't set an href
         */
        if(!id) {
            return
        }
    
        /**
         * also return if it is an external link
         * for example forum link
         */
        if(id.charAt(0) !== "#") {
            return;
        }
        
        event.preventDefault();
        
        landingController.scrollTo(id);
    
        /**
         * also, add the anchor to the url if possible
         */
        if(window.history && window.history.pushState) {
            history.pushState("", document.title, id);
        }
    }
    
    /**
     * check after page-load if an anchor is in url. if yes, scroll to the linked section
     */
    function scrollToSectionAfterLoad() {
        if(location.hash.charAt(0) === "#") {
            landingController.scrollTo(location.hash);
        }
    }
    
    return {
        start: start
    }
})();

steeltech.init.start();